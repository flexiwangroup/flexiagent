Welcome to flexiAgent's documentation!
======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. autosummary::
   :toctree: _autosummary

   fwagent
   fwutils
   fwtranslate_add_link_monitor
   fwtranslate_add_switch
   fwtranslate_add_routing_general
   fwtranslate_add_routing_filter
   fwtranslate_add_dhcp_config
   fwtranslate_add_vxlan_config
   fwtranslate_add_routing_bgp
   fwtranslate_add_tunnel
   fwtranslate_add_multilink_policy
   fwtranslate_add_qos_traffic_map
   fwtranslate_add_qos_policy
   fwtranslate_add_route
   fwtranslate_add_vrrp_group
   fwtranslate_add_app_config
   fwtranslate_add_app_install
   fwtranslate_add_notifications_config
   fwtranslate_add_application
   fwtranslate_add_firewall_policy
   fwtranslate_add_interface
   fwtranslate_add_ospf
   fwtranslate_add_lte
   fwtranslate_add_lan_nat_policy
   fwtranslate_add_cpu_info
   fwtranslate_start_router
   fwtranslate_revert
   fwroutes
   fwpolicies
   fwnetplan
   fwnotifications
   fwmultilink
   fwlte
   fwthread
   fwlog
   fw_os_utils
   fwgrub
   loadsimulator
   fwstun
   fwikev2
   fwobject
   fwsqlitedict
   fwcli_configure_pppoe
   fwcli_configure_router
   fwcli_configure_jobs
   fwdhcp_server
   fwrouter_api
   fwtunnel_stats
   vpp_papi_dummy
   fw_acl_command_helpers
   fwexception
   fwwan_monitor
   fw_traffic_identification
   fw_nat_command_helpers
   ubuntu
   fwsystem_checker_common
   fwsystem_checker
   fwfrr
   fwqos
   fwapplications_api
   fwwebsocket
   fwcfg_database
   os_api
   fwglobals
   fwrouter_cfg
   vpp_api
   fwcfg_request_handler
   fwstats
   fwfirewall
   fwpppoe
   fwjobs
   fwwifi
   fwsystem_api
   fw_vpp_coredump_utils
   fwagent_api
   fwwatchdog
   fwmessage_handler
   fwrecover_connection
   fwsystem_cfg
   fwapplications_cfg
   fwagent_cli
   dpdk-devbind


Indices and tables
==================
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
