#! /usr/bin/python3

################################################################################
# flexiWAN SD-WAN software - flexiEdge, flexiManage.
# For more information go to https://flexiwan.com
#
# Copyright (C) 2019  flexiWAN Ltd.
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
################################################################################

import fwglobals

def add_ospf(params):
    """OSPF configuration to frr.

    :param cmd_list:            List of commands.

    :returns: None.
    """
    cmd_list = []

    # routerId
    router_id = params.get('routerId')
    if router_id:
        cmd = {}
        cmd['cmd'] = {}
        cmd['cmd']['func']   = "frr_vtysh_run"
        cmd['cmd']['module'] = "fwutils"
        cmd['cmd']['descr']  =  f"add router-id {router_id} to OSPF"
        cmd['cmd']['params'] = {
                    'commands'   : ["router ospf", f"ospf router-id {router_id}"],
                    'restart_frr': True,
                    'on_error_commands': ["router ospf", f"no ospf router-id {router_id}"],
        }
        cmd['revert'] = {}
        cmd['revert']['func']   = "frr_vtysh_run"
        cmd['revert']['module'] = "fwutils"
        cmd['revert']['params'] = {
                    'commands'   : ["router ospf", f"no ospf router-id {router_id}"],
                    'restart_frr': True,
        }
        cmd['revert']['descr']   =  f"remove routerId {router_id} from OSPF"
        cmd_list.append(cmd)

    redistribute_bgp = params.get('redistributeBgp')
    if redistribute_bgp:
        cmd = {}
        cmd['cmd'] = {}
        cmd['cmd']['func']   = "frr_vtysh_run"
        cmd['cmd']['module'] = "fwutils"
        cmd['cmd']['descr']  =  "add 'redistribute bgp' to OSPF configuration"
        cmd['cmd']['params'] = {
                    'commands'   : ["router ospf", "redistribute bgp"],
                    'on_error_commands': ["router ospf", "no redistribute bgp"],
        }
        cmd['revert'] = {}
        cmd['revert']['func']   = "frr_vtysh_run"
        cmd['revert']['module'] = "fwutils"
        cmd['revert']['params'] = {
                    'commands'   : ["router ospf", "no redistribute bgp"],
        }
        cmd['revert']['descr']   =  "remove 'redistribute bgp' from OSPF configuration"
        cmd_list.append(cmd)

    # "custom" includes a list of commands for FRR CLI (vtysh) under the "routr ospf"
    custom_commands = params.get('custom', [])
    if custom_commands:
        frr_commands = ["router ospf"]
        remove_commands = ["router ospf"]
        for custom_command in custom_commands:
            frr_commands.append(custom_command)
            remove_commands.append(f'no {custom_command}')

        cmd = {}
        cmd['cmd'] = {}
        cmd['cmd']['func']   = "frr_vtysh_run"
        cmd['cmd']['module'] = "fwutils"
        cmd['cmd']['descr']  =  "add custom OSPF commands"
        cmd['cmd']['params'] = {
                    'commands'   : frr_commands,
                    'on_error_commands': remove_commands,
        }
        cmd['revert'] = {}
        cmd['revert']['func']   = "frr_vtysh_run"
        cmd['revert']['module'] = "fwutils"
        cmd['revert']['params'] = {
                    'commands'   : remove_commands,
        }
        cmd['revert']['descr']   =  "remove custom OSPF commands"
        cmd_list.append(cmd)

    # Add logical interface IP to the network
    if fwglobals.g.cfg.DEVICE_LOGICAL_INTERFACE_IP:
        cmd = {}
        cmd['cmd'] = {}
        cmd['cmd']['func']   = "frr_vtysh_run"
        cmd['cmd']['module'] = "fwutils"
        cmd['cmd']['params'] = {
            'commands': [
                "router ospf",
                f"network {fwglobals.g.cfg.DEVICE_LOGICAL_INTERFACE_IP} area 0"
            ]
        }
        cmd['cmd']['descr']   = "add logical interface %s to ospf" %\
            fwglobals.g.cfg.DEVICE_LOGICAL_INTERFACE_IP

        cmd['revert'] = {}
        cmd['revert']['func']   = "frr_vtysh_run"
        cmd['revert']['module'] = "fwutils"
        cmd['revert']['params'] = {
            'commands': [
                "router ospf",
                f"no network {fwglobals.g.cfg.DEVICE_LOGICAL_INTERFACE_IP} area 0"
            ],
        }
        cmd['revert']['descr']  = "remove logical interface %s from ospf" %\
            fwglobals.g.cfg.DEVICE_LOGICAL_INTERFACE_IP
        cmd_list.append(cmd)

    return cmd_list

def get_request_key(params):
    """Get add-ospf-config command.

    :param params:        Parameters from flexiManage.

    :returns: add-ospf-config command.
    """
    key = 'add-ospf-config'
    return key
